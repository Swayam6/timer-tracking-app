import ReactDOM from 'react-dom';
import TimerDashboard from './components/TimerDashboard';

ReactDOM.render(<TimerDashboard />, document.getElementById('root'));
